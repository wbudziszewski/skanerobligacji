SkanerObligacji

A utility that aims to construct bond yield curves using actual transaction data and using Nelsson-Siegel-Svensson or polynomial splines methods.