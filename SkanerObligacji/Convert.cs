﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkanerObligacji
{
    public static class Convert
    {


        public static double? GetDouble(string input, string culture)
        {
            input = input.Trim();
            if (input == "-")
            {
                return null;
            }
            try
            { 
                return Double.Parse(input, culture == null ? CultureInfo.InvariantCulture: new CultureInfo(culture));
            }
            catch
            {
                return null;
            }
        }

        public static int? GetInt(string input, string culture)
        {
            input = input.Trim();
            if (input == "-")
            {
                return null;
            }
            try
            {
                return Int32.Parse(input, culture == null ? CultureInfo.InvariantCulture : new CultureInfo(culture));
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? GetDateTime(string input, string culture)
        {
            input = input.Trim();
            if (input == "-")
            {
                return null;
            }
            try
            {
                return DateTime.ParseExact(input, "yyyy-MM-dd", culture == null ? CultureInfo.InvariantCulture : new CultureInfo(culture));
            }
            catch
            {
                return null;
            }
        }
    }
}
