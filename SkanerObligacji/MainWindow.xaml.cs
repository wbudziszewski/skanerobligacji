﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkanerObligacji
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static Financial.CurveModelType[] cmt = new Financial.CurveModelType[] { Financial.CurveModelType.NelsonSiegelSvensson, Financial.CurveModelType.CubicSplines };

        public ObservableCollection<Financial.Curve> Curves { get; set; } = new ObservableCollection<Financial.Curve>();

        public ObservableCollection<Bond> Bonds { get; set; } = new ObservableCollection<Bond>();

        public PlotModel PlotModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeCurveDisplay();
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            // Add government curve
            Financial.Curve newCurve;
            Curves.Clear();
            newCurve = new Financial.Curve(cmt[comboCurveMethod.SelectedIndex], "skarbowa");
            Curves.Add(newCurve);
            newCurve = new Financial.Curve(cmt[comboCurveMethod.SelectedIndex], "korporacyjna");
            Curves.Add(newCurve);

            var bondsSrcGovt = Import.ImportBonds("http://www.gpwcatalyst.pl/notowania_obligacji_skarbowych_full", "skarbowa").ToList();
            if (comboFilter.SelectedIndex > 1)
            {
                bondsSrcGovt = bondsSrcGovt.Where(x => x.Segment == comboFilter.Text).ToList();
            }
            else if (comboFilter.SelectedIndex == 1)
            {
                bondsSrcGovt = bondsSrcGovt.Where(x => x.Segment.StartsWith("GPW")).ToList();
            }

            var bondsSrcCorp = Import.ImportBonds("http://www.gpwcatalyst.pl/notowania_obligacji_korporacyjnych_full", "korporacyjna").ToList();
            if (comboFilter.SelectedIndex > 1)
            {
                bondsSrcCorp = bondsSrcCorp.Where(x => x.Segment == comboFilter.Text).ToList();
            }
            else if (comboFilter.SelectedIndex == 1)
            {
                bondsSrcCorp = bondsSrcCorp.Where(x => x.Segment.StartsWith("GPW")).ToList();
            }

            var bondsSrc = new List<Bond>();
            bondsSrc.AddRange(bondsSrcGovt);
            bondsSrc.AddRange(bondsSrcCorp);
            Bonds = new ObservableCollection<Bond>(bondsSrc);

            List.ItemsSource = Bonds;

            // Get details
            foreach (var b in Bonds)
            {
                List<KeyValuePair<string, string>> headers = new List<KeyValuePair<string, string>>();
                headers.Add(new KeyValuePair<string, string>("Accept", "application/json, text/javascript, */*; q=0.01"));
                headers.Add(new KeyValuePair<string, string>("Host", "gpw.notoria.pl"));
                headers.Add(new KeyValuePair<string, string>("Origin", "http://www.gpwcatalyst.pl"));
                headers.Add(new KeyValuePair<string, string>("Referer", "http://www.gpwcatalyst.pl/instrument?nazwa=" + b.Name));
                headers.Add(new KeyValuePair<string, string>("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"));
                List<KeyValuePair<string, string>> formData = new List<KeyValuePair<string, string>>();
                string jsonSrc = Budziszewski.Web.DownloadPost("https://gpw.notoria.pl/", "widgets/bonds/profile.php?id=" + b.Name, headers, formData, false);
                Budziszewski.JSON json = new Budziszewski.JSON(jsonSrc);
                await b.GetDetailsAsync(json.Root);
                await b.RecalculateAsync();
                RecalculateScores(b.Type);
                await SetCurveNodes(b.Type);
                //await AddPointToCurve(b.Type, b.Tenor, b.IndicativeYield);
            }
        }

        void RecalculateScores(string type)
        {
            var liquidities = Bonds.Where(x => x.IsCalculated && x.Type == type).Select(x => x.AbsoluteLiquidityScore).Where(x => !Double.IsNaN(x) && x >= 0).OrderByDescending(x => x);
            var minLiquidity = liquidities.ElementAtOrDefault(liquidities.Count() - (int)(liquidities.Count() / 10.0));
            var maxLiquidity = liquidities.ElementAtOrDefault((int)(liquidities.Count() / 10.0));
            foreach (var b in Bonds)
            {
                if (!b.IsCalculated || b.Type != type) continue;
                if (double.IsNaN(b.AbsoluteLiquidityScore))
                {
                    b.LiquidityScore = 0.1;
                }
                else if (b.AbsoluteLiquidityScore < 0)
                {
                    b.LiquidityScore = 0.25;
                }
                else
                {
                    if (b.AbsoluteLiquidityScore < minLiquidity)
                    {
                        b.LiquidityScore = 0.5;
                    }
                    else if (b.AbsoluteLiquidityScore > maxLiquidity)
                    {
                        b.LiquidityScore = 0.5;
                    }
                    else if (maxLiquidity - minLiquidity != 0)
                    {
                        b.LiquidityScore = (maxLiquidity - b.AbsoluteLiquidityScore) / (maxLiquidity - minLiquidity);
                        b.LiquidityScore *= 0.5;
                        b.LiquidityScore += 0.5;
                    }
                    else
                    {
                        b.LiquidityScore = 1.0;
                    }
                }
                b.Refresh();
            }

            var sanities = Bonds.Where(x => x.IsCalculated && x.Type == type).Select(x => x.AbsoluteSanityScore).Where(x => !Double.IsNaN(x)).OrderByDescending(x => x);
            double avgSanity;
            if (sanities.Count() < 30)
            {
                avgSanity = sanities.Average();
            }
            else
            {
                double count = sanities.Count();
                avgSanity = sanities.Skip((int)(count / 10.0)).Take(sanities.Count() - (int)(count / 10.0)).Average();
            }
            foreach (var b in Bonds)
            {
                if (!b.IsCalculated || b.Type != type) continue;
                if (double.IsNaN(b.AbsoluteSanityScore))
                {
                    b.SanityScore = 0;
                }
                else
                {
                    b.SanityScore = Math.Pow(1 + (Math.Abs(b.AbsoluteSanityScore - avgSanity) / avgSanity), -16);
                }
                b.Refresh();
            }
        }

        private async Task SetCurveNodes(string type)
        {
            var bonds = Bonds.Where(x => x.Type == type && x.ValidNode);
            var nodes = bonds.Select(x => x.GetNode());
            switch (type)
            {
                case "skarbowa":
                    await Task.Run(() =>
                    {
                        Curves[0].SetNodes(nodes);
                        DisplayCurve(type, Curves[0], Colors.Navy);
                    });
                    break;
                case "korporacyjna":
                    await Task.Run(() =>
                    {
                        Curves[1].SetNodes(nodes);
                        DisplayCurve(type, Curves[1], Colors.Red);
                    });
                    break;
                default:
                    break;
            }
        }

        private async Task AddPointToCurve(string type, double? tenor, double? indicativeYield)
        {
            if (!tenor.HasValue || !indicativeYield.HasValue) return;
            switch (type)
            {
                case "skarbowa":
                    await Task.Run(() => Curves[0].AddNode(tenor.Value, indicativeYield.Value));
                    DisplayCurve(type, Curves[0], Colors.Navy);
                    break;
                case "korporacyjna":
                    await Task.Run(() => Curves[1].AddNode(tenor.Value, indicativeYield.Value));
                    DisplayCurve(type, Curves[1], Colors.Red);
                    break;
                default:
                    break;
            }
        }

        void InitializeCurveDisplay()
        {
            PlotModel = new PlotModel();
            Oxy.DataContext = PlotModel;
            var xAxis = new OxyPlot.Axes.LinearAxis()
            {
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                MinorStep = 1,
                MajorStep = 5,
                Minimum = 0,
                Maximum = 25

            };
            PlotModel.Axes.Add(xAxis);
            var yAxis = new OxyPlot.Axes.LinearAxis()
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                MinorStep = 0.0025,
                MajorStep = 0.01,
                Minimum = -0.025,
                Maximum = 0.075
            };
            PlotModel.Axes.Add(yAxis);
            PlotModel.Series.Clear();

            PlotModel.Series.Add(new LineSeries()
            {
                Color = OxyColor.FromRgb(Colors.Navy.R, Colors.Navy.G, Colors.Navy.B),
                Tag = "skarbowa"
            });
            PlotModel.Series.Add(new LineSeries()
            {
                Color = OxyColor.FromRgb(Colors.Red.R, Colors.Red.G, Colors.Red.B),
                Tag = "korporacyjna"
            });
            PlotModel.Series.Add(new ScatterSeries()
            {
                MarkerFill = OxyColor.FromRgb(Colors.Navy.R, Colors.Navy.G, Colors.Navy.B),
                MarkerType = MarkerType.Circle,
                Tag = "skarbowa"
            });
            PlotModel.Series.Add(new ScatterSeries()
            {
                MarkerFill = OxyColor.FromRgb(Colors.Red.R, Colors.Red.G, Colors.Red.B),
                MarkerType = MarkerType.Circle,
                Tag = "korporacyjna"
            });
        }

        void DisplayCurve(string id, Financial.Curve curve, Color color)
        {
            Series s = PlotModel.Series.First(x => x.Tag.ToString() == id && x is LineSeries);
            (s as LineSeries).Points.Clear();
            foreach (var p in curve.GetCurvePoints(0, 25, 0.1))
            {
                (s as LineSeries).Points.Add(new DataPoint(p.X, p.Y));
            }
            s = PlotModel.Series.First(x => x.Tag.ToString() == id && x is ScatterSeries);
            (s as ScatterSeries).Points.Clear();
            foreach (var p in curve.GetNodes())
            {
                (s as ScatterSeries).Points.Add(new ScatterPoint(p.Maturity, p.Value, p.LiquidityScore, p.SanityScore));
            }
            PlotModel.InvalidatePlot(true);
        }

    }
}
