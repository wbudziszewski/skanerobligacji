﻿using Financial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkanerObligacji
{
    public class Bond : INotifyPropertyChanged
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public string Issuer { get; set; }

        public string Segment { get; set; }

        public string Currency { get; set; }

        public double? IndicativePrice { get; set; }

        public DateTime? LastTransactionDate { get; set; }

        public double? LastTransactionPrice { get; set; }

        public int? PurchaseOfferVolume { get; set; }

        public double? PurchaseOfferPrice { get; set; }

        public int? SaleOfferVolume { get; set; }

        public double? SaleOfferPrice { get; set; }

        public double? OfferSpread { get; set; }

        public double? NominalValue { get; set; }

        public double? IssueTotalValue { get; set; }

        public DateTime? MaturityDate { get; set; }

        public DateTime? CurrentPeriodStart { get; set; }

        public DateTime? CurrentPeriodEnd { get; set; }

        public string CouponType { get; set; }

        public string CouponBase { get; set; }

        public double? CouponMargin { get; set; }

        public double? CouponCurrentRate { get; set; }

        public int? CouponFreq { get; set; }

        public string DefaultStatus { get; set; }

        public double? IndicativeYield { get; private set; }

        public double? PurchaseYield { get; private set; }

        public double? SaleYield { get; private set; }

        public double? MDuration { get; private set; }

        public double? Tenor { get; private set; }

        public DateTime? LastRefresh { get; private set; }

        public bool IsCalculated { get; private set; }

        public double AbsoluteLiquidityScore { get; private set; }

        public double LiquidityScore { get; set; }

        public double AbsoluteSanityScore { get; private set; }

        public double SanityScore { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool ValidNode { get { return Tenor != null && IndicativeYield != null; } }

        public void GetDetails(Budziszewski.JSONObject src)
        {
            var info = src.Children[0].Children.Where(x => x is Budziszewski.JSONAttribute).Cast<Budziszewski.JSONAttribute>();

            try
            {
                NominalValue = Convert.GetDouble(info.First(x => x.Token == "issue_price").Value.ToString(), "en-US");
                IssueTotalValue = Convert.GetDouble(info.First(x => x.Token == "issue_vol").Value.ToString(), "en-US") * NominalValue;
            }
            catch
            {
                NominalValue = null;
                IssueTotalValue = null;
            }
            try
            {
                MaturityDate = Convert.GetDateTime(info.First(x => x.Token == "maturity_date").Value.ToString(), "en-US");
                CurrentPeriodStart = Convert.GetDateTime(info.First(x => x.Token == "FCD").Value.ToString(), "en-US");
                CurrentPeriodEnd = Convert.GetDateTime(info.First(x => x.Token == "CPD").Value.ToString(), "en-US");
            }
            catch
            {
                MaturityDate = null;
                CurrentPeriodStart = null;
                CurrentPeriodEnd = null;
            }

            object coupbase = info.FirstOrDefault(x => x.Token == "ir_base")?.Value;
            if (coupbase == null)
            {
                CouponType = "stały";
                CouponBase = null;
                CouponMargin = null;
            }
            else
            {
                CouponType = "zmienny";
                CouponBase = coupbase.ToString().Trim();
                try
                {
                    CouponMargin = Convert.GetDouble(info.First(x => x.Token == "ir_margin").Value.ToString(), "en-US");
                }
                catch
                {
                    CouponMargin = 0;
                }
            }

            try
            {
                CouponCurrentRate = Convert.GetDouble(info.First(x => x.Token == "ir_real").Value.ToString(), "en-US");
            }
            catch
            {
                try
                {
                    CouponCurrentRate = Convert.GetDouble(info.First(x => x.Token == "ir_margin").Value.ToString(), "en-US");
                }
                catch
                {
                    CouponCurrentRate = CouponMargin;
                }
            }

            try
            {
                CouponFreq = Convert.GetInt(info.First(x => x.Token == "pnum").Value.ToString(), "en-US");
            }
            catch
            {
                CouponFreq = null;
            }
            if (CouponFreq == null || CouponFreq == 0)
            {
                CouponCurrentRate = 0;
                CouponFreq = 1;
            }

            try
            {
                DefaultStatus = info.First(x => x.Token == "payment_status").Value.ToString().Trim();
            }
            catch
            {
                DefaultStatus = "brak informacji";
            }

            LastRefresh = DateTime.Now;
            RaisePropertyChanged(null);
        }

        public async Task GetDetailsAsync(Budziszewski.JSONObject src)
        {
            await Task.Run(() => { GetDetails(src); });
        }

        public void Recalculate()
        {
            DateTime now = DateTime.Now;

            if (MaturityDate == null || CouponCurrentRate == null || CouponFreq == null)
            {
                IndicativeYield = null;
                PurchaseYield = null;
                SaleYield = null;
                MDuration = null;
            }
            else
            {
                if (IndicativePrice == null)
                {
                    IndicativeYield = null;
                    MDuration = null;
                }
                else
                {
                    try
                    {
                        IndicativeYield = Budziszewski.FixedIncome.Yield(now, MaturityDate.Value, CouponCurrentRate.Value, IndicativePrice.Value, 100, CouponFreq.Value, Budziszewski.DayCountConvention.Actual_Actual_Excel);
                        MDuration = Budziszewski.FixedIncome.MDuration(now, MaturityDate.Value, CouponCurrentRate.Value, IndicativeYield.Value, 100, CouponFreq.Value, Budziszewski.DayCountConvention.Actual_Actual_Excel);
                    }
                    catch
                    {
                        IndicativeYield = null;
                        MDuration = null;
                    }
                }

                if (PurchaseOfferPrice == null)
                {
                    PurchaseYield = null;
                }
                else
                {
                    try
                    {
                        PurchaseYield = Budziszewski.FixedIncome.Yield(now, MaturityDate.Value, CouponCurrentRate.Value, PurchaseOfferPrice.Value, 100, CouponFreq.Value, Budziszewski.DayCountConvention.Actual_Actual_Excel);
                    }
                    catch
                    {
                        PurchaseYield = null;
                    }
                }

                if (SaleOfferPrice == null)
                {
                    SaleYield = null;
                }
                else
                {
                    try
                    {
                        SaleYield = Budziszewski.FixedIncome.Yield(now, MaturityDate.Value, CouponCurrentRate.Value, SaleOfferPrice.Value, 100, CouponFreq.Value, Budziszewski.DayCountConvention.Actual_Actual_Excel);
                    }
                    catch
                    {
                        SaleYield = null;
                    }
                }

                if (MaturityDate == null || CouponFreq == null)
                {
                    Tenor = null;
                }
                else
                {
                    try
                    {
                        Tenor = Budziszewski.DayCount.GetTenor(now, MaturityDate.Value, CouponFreq.Value, Budziszewski.DayCountConvention.Actual_Actual_Excel);
                    }
                    catch
                    {
                        Tenor = null;
                    }
                }
            }

            // Calculate scores
            if ((SaleOfferPrice == null && PurchaseOfferPrice == null) || IndicativePrice == null)
            {
                AbsoluteLiquidityScore = double.NaN;
            }
            else if (SaleOfferPrice == null || PurchaseOfferPrice == null)
            {
                AbsoluteLiquidityScore = -1;
            }
            else
            {
                AbsoluteLiquidityScore = Math.Abs((PurchaseOfferPrice.Value - SaleOfferPrice.Value) / (IndicativePrice.Value));
            }
            if (IndicativeYield == null)
            {
                AbsoluteSanityScore = double.NaN;
            }
            else
            {
                AbsoluteSanityScore = IndicativeYield.Value;
            }

            LastRefresh = DateTime.Now;
            IsCalculated = true;
            RaisePropertyChanged(null);
        }

        public void Refresh()
        {
            RaisePropertyChanged(null);
        }

        public async Task RecalculateAsync()
        {
            await Task.Run(() => { Recalculate(); });
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public CurveNode GetNode()
        {
            return new CurveNode(Tenor ?? 0, IndicativeYield ?? 0, LiquidityScore, SanityScore);
        }
    }
}
