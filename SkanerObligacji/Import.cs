﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkanerObligacji
{
    public static class Import
    {
        public static IEnumerable<Bond> ImportBonds(string path, string type)
        {
            string page = Budziszewski.Web.DownloadWebPage(path);

            var html = new HtmlAgilityPack.HtmlDocument();
            html.LoadHtml(page);

            string issuer = "";
            string name = "";
            int issuerRowsLeft = 0;
            int nameRowsLeft = 0;
            int col = 0;
            int tidx = 0;
            string currency = "";

            var tables = html.DocumentNode.ChildNodes["html"].ChildNodes["body"].ChildNodes.Where(x => x.Name == "table");

            foreach (var t in tables)
            {
                currency = tidx++ > 0 ? "EUR" : "PLN";
                issuerRowsLeft = 0;
                nameRowsLeft = 0;

                var rows = t.ChildNodes["tbody"].ChildNodes.Where(x => x.Name == "tr").ToList();

                foreach (var r in rows)
                {
                    var cells = r.ChildNodes.Where(x => x.Name == "td").ToArray();
                    col = 0;

                    if (--issuerRowsLeft < 1)
                    {
                        issuer = cells[col].ChildNodes[1].InnerText.Trim();
                        if (cells[col].GetAttributeValue("rowspan", null) == null)
                        {
                            issuerRowsLeft = 1;
                        }
                        else
                        { 
                            issuerRowsLeft = Int32.Parse(cells[col].Attributes["rowspan"].Value);
                        }
                        col++;
                    }

                    if (--nameRowsLeft < 1)
                    {
                        name = cells[col].ChildNodes[1].InnerText.Trim();
                        if (cells[col].GetAttributeValue("rowspan", null) == null)
                        {
                            nameRowsLeft = 1;
                        }
                        else
                        {
                            nameRowsLeft = Int32.Parse(cells[col].Attributes["rowspan"].Value);
                        }
                        col++;
                    }

                    Bond b = new Bond();
                    b.Issuer = issuer;
                    b.Name = name;
                    b.Type = type;
                    b.Currency = currency;

                    col++;
                    b.Segment = cells[col++].InnerText.Replace("&nbsp;", " ");
                    col++;
                    b.IndicativePrice = Convert.GetDouble(cells[col++].InnerText, "pl-PL");
                    col++;
                    col++;
                    col++;
                    b.LastTransactionDate = Convert.GetDateTime(cells[col++].InnerText, "pl-PL");
                    col++;
                    b.LastTransactionPrice = Convert.GetDouble(cells[col++].InnerText, "pl-PL");
                    col++;
                    col++;
                    b.PurchaseOfferVolume = Convert.GetInt(cells[col++].InnerText, "pl-PL");
                    b.PurchaseOfferPrice = Convert.GetDouble(cells[col++].InnerText, "pl-PL");
                    b.SaleOfferPrice = Convert.GetDouble(cells[col++].InnerText, "pl-PL");
                    b.SaleOfferVolume = Convert.GetInt(cells[col++].InnerText, "pl-PL");
                    col++;
                    col++;
                    col++;
                    col++;

                    if (b.PurchaseOfferPrice != null && b.SaleOfferPrice != null)
                    {
                        b.OfferSpread = Math.Round(Math.Abs(b.PurchaseOfferPrice.Value - b.SaleOfferPrice.Value), 8);
                    }
                    else
                    {
                        b.OfferSpread = null;
                    }

                    yield return b;
                }

            }
        }
    }
}
